@extends('layouts.site')

@section('content')
<div class="flex justify-center">
    {{ html()->form('POST', route('login'))->class('bg-white shadow-md rounded px-8 pt-6 pb-8 my-6 align-middle')->attributes(['style' => 'width:420px; max-width: 100%;'])->open() }}
        <h1 class="font-thin text-center mb-6">{{ __('Acesse sua conta') }}</h1>
        
        {{ form_field()->required()->email('email', 'E-mail') }}
        {{ form_field()->required()->password('password', 'Senha') }}

        <div class="flex justify-center my-8">
            {{ form_field()->submit('Entrar')->addClass('button-l') }}
        </div>

        <div class="flex justify-center my-8">
            <label class="w-3/4 ml-auto">
            </label>
        </div>

        <div class="text-center">
            <p class="text-sm">
                <a href="{{ route('register') }}" class="text-grey">{{ __('Não tem conta? Crie agora mesmo!') }}</a>
            </p>
            <div class="text-sm mt-2">
                <a class="text-grey" href="{{ route('password.request') }}">
                    {{ __('Esqueceu sua senha?') }}
                </a>
            </div>
        </div>
    {{ html()->form()->close() }}
</div>
@endsection
