<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;

class CreateProjectHandler extends Controller
{
    public function __invoke(Request $request)
    {
        return view('projects.create')->with('project', new Project);
    }
}
