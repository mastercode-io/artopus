<?php

namespace App\Http\Controllers;

class HomeHandler extends Controller
{
    /**
     * Show the website home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return view('home');
    }
}
