<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 31/08/17
 * Time: 02:21
 */

namespace App\Jobs;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

trait ValidatesJob
{
    protected function validate(array $rules): Collection
    {
        Validator::make($this->data, $rules)->validate();
        return collect($this->data)->only(array_keys($rules));
    }
}