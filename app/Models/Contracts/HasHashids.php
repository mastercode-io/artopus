<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 03/10/17
 * Time: 01:34
 */

namespace App\Models\Contracts;

interface HasHashids
{
    /**
     * Generates a Hash Id based on model's id.
     *
     * @return string
     */
    public function makeHashid(): string;

    /**
     * Check if the provided hashid matches the model's id.
     *
     * @param string $hashid
     * @return bool
     */
    public function checkHashid(string $hashid): bool;
}
