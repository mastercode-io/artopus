{{ html()->modelForm($project, $method, $action)->class('bg-white shadow-md rounded px-8 pt-6 pb-8 my-6 align-middle')->attributes(['style' => 'width:420px; max-width: 100%;'])->open() }}

{{ form_field()->required()->text('name', 'Nome do projeto') }}
{{ form_field()->required()->text('description', 'Descrição') }}

<div class="flex justify-center my-8">
    {{ form_field()->submit($project->exists ? 'Atualizar projeto' : 'Criar projeto')->addClass('button-l') }}
</div>

<div class="flex justify-center my-8">
    <a href="{{ route('dashboard') }}" class="text-sm text-grey-dark">{{ __('Voltar à lista') }}</a>
</div>

{{ html()->form()->close() }}