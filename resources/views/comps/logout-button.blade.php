<a href="{{ route('logout') }}"
    class="no-underline hover:underline text-grey-darker text-sm"
    onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>