<div class="max-w-2xl mx-auto flex justify-center">
    <aside class="flex-auto max-w-xs pr-8">
        <a href="{{ route('projects.edit', $project) }}" class="font-bold text-md text-black no-underline">{{ $project->name }}</a>
        @foreach ($project->sections as $section)
            <div class="pt-2">
                <a href="{{ route('sections.edit', [$project, $section]) }}" class="text-sm text-grey-dark no-underline {{ active('sections.edit', [$project, $section]) }}">
                    {{ $section->small_title }}
                </a>
            </div>
        @endforeach

        {{ html()->form('POST', route('sections.store', $project))->class('w-full mt-8 flex')->open() }}
            <div class="flex-grow field">
                {{ html()->text('title')->class('field__input')->required()->placeholder(__('Nova seção')) }}
                {{ form_field()->submit('+')->attributes(['style' => 'visibility:hidden']) }}
            </div>
        {{ html()->form()->close() }}
    </aside>
    <main class="flex-grow bg-white shadow-md rounded p-4">
        {{ $slot }}
    </main>
</div>