<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class StoreProjectHandler extends Controller
{
    public function __invoke(Request $request, Project $projects)
    {
        $input = $request->validate([
            'name'        => 'required|max:255',
            'description' => 'nullable',
        ]);

        $item = $projects->create(
            array_merge($input, ['owner_id' => $request->user()->getKey()])
        );

        return redirect()->route('projects.edit', $item);
    }
}
