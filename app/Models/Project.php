<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\Scopes\ScopesByUser;
use App\Models\Helpers\HasUserComparisons;
use App\Models\Helpers\RoutesByHashid;

/**
 * @property \Carbon\Carbon $deleted_at
 * @property mixed          $owner
 * @property mixed          $sections
 * @property mixed          owner_id
 * @property \Carbon\Carbon $created_at
 * @property int            $id
 * @property \Carbon\Carbon $updated_at
 */
class Project extends BaseModel
{
    use ScopesByUser, HasUserComparisons, RoutesByHashid;

    protected $fillable = [
        'name',
        'description',
        'owner_id',
    ];

    /**
     * The user who created this project.
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    /**
     * List of sections belonging to this section.
     *
     * @return mixed
     */
    public function sections(): HasMany
    {
        return $this->hasMany(Section::class);
    }

    /**
     * Get a list of projects created by the specified user.
     *
     * @param integer $userId
     * @return void
     */
    public function getByUserId(int $userId)
    {
        return $this->createdBy($userId)->get();
    }
}
