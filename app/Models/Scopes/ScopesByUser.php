<?php

namespace App\Models\Scopes;

trait ScopesByUser
{
    /**
     * Filter projects by owner.
     *
     * @param        $query
     * @param string $userId
     * @return mixed
     */
    public function scopeCreatedBy($query, string $userId)
    {
        return $query->where('owner_id', $userId);
    }
}
