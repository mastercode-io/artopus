@extends('layouts.site')

@section('content')
<main class="flex justify-center">
    {{ html()->form('POST', route('register'))->class('bg-white shadow-md rounded px-8 pt-6 pb-8 my-6 align-middle')->attributes(['style' => 'width:420px; max-width: 100%;'])->open() }}
        <h1 class="font-thin text-center mb-6">{{ __('Crie sua conta') }}</h1>
        
        {{ form_field()->required()->text('name', 'Nome') }}
        {{ form_field()->required()->email('email', 'E-mail') }}
        {{ form_field()->required()->password('password', 'Senha') }}
        {{ form_field()->required()->password('password_confirmation', 'Confirme sua senha') }}

        <div class="flex justify-center my-8">
            {{ form_field()->submit('Começar agora!')->addClass('button-l') }}
        </div>

        <div class="text-center">
            <p class="text-sm">
                <a href="{{ route('login') }}" class="text-grey">{{ __('Já tem uma conta? Faça o login.') }}</a>
            </p>
        </div>
    {{ html()->form()->close() }}
</main>
@endsection
