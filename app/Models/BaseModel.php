<?php

namespace App\Models;

use App\Models\Contracts\Hashable;
use App\Models\Contracts\HasHashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModel extends Model implements HasHashids
{
    use Hashable, SoftDeletes;
}
