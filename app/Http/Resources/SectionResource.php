<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SectionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->getKey(),
            'title'        => $this->title,
            'file_content' => $this->file_content,
            'status'       => $this->status,
            'created_at'   => $this->created_at,
        ];
    }
}
