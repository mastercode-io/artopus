<?php

namespace App\Jobs\File;

use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class WriteFileCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var string
     */
    private $file;
    /**
     * @var
     */
    private $content;

    /**
     * Create a new job instance.
     *
     * @param string $file
     * @param        $content
     */
    public function __construct(string $file, $content)
    {
        $this->file = $file;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!Storage::cloud()->put($this->file, $this->content)) {
            // TODO: throw exception
        }

        // TODO: event()

        return $this->file;
    }
}
