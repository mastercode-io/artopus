<?php

namespace App\Http\Controllers\Sections;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Section;

class EditSectionHandler extends Controller
{
    public function __invoke(Project $project, Section $section)
    {
        return view('sections.edit')
            ->with('project', $project)
            ->with('section', $section);
    }
}
