<?php

if (!function_exists('date_to_string')) {
    /**
     * Transform a date to a general string format.
     *
     * @param mixed $date
     * @return void
     */
    function date_to_string($date): string
    {
        if (is_null($date)) {
            return '';
        }

        return $date->toIso8601String();
    }
}

if (!function_exists('date_for_humans')) {
    /**
     * Transform a date to a more human-readable format, like "2 minutes ago".
     *
     * @param mixed $date
     * @return void
     */
    function date_for_humans($date): string
    {
        if (is_null($date)) {
            return '';
        }

        return $date->diffForHumans();
    }
}

if (!function_exists('form_field')) {
    /**
     * Get a instance of FormField.
     *
     * @return \App\Utils\FormField
     */
    function form_field()
    {
        return app(\App\Utils\FormField::class);
    }
}

if (!function_exists('render_error')) {
    /**
     * Render a error message.
     *
     * @param string $field
     * @return string
     */
    function render_error(string $field): string
    {
        $errors = session()->get('errors');

        if (!$errors) {
            return '';
        }

        return $errors->first($field, '<span class="text-red-dark text-sm mt-2">:message</span>');
    }
}

if (!function_exists('active')) {
    /**
     * Determine if the current page matches the specified route.
     *
     * @param string $route
     * @param ?array $params
     * @return void
     */
    function active(string $route, ?array $params = null)
    {
        $path = str_replace(config('app.url') . '/', '', route($route, $params));

        if (request()->is($path)) {
            echo 'active text-brand';
        }

        echo '';
    }
}
