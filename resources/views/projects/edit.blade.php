@extends('layouts.app')

@section('content')
    <div class="max-w-2xl mx-auto">
        @component('comps.section-title') {{ $project->name }} @endcomponent
    </div>
    @component('comps.content-editing', ['project' => $project])
        {{ html()->modelForm($project, 'PUT', route('projects.update', $project))->open() }}

            {{ form_field()->required()->text('name', 'Título do projeto') }}
            {{ form_field()->required()->text('description', 'Descrição') }}

            <div class="flex justify-center my-8">
                {{ form_field()->submit('Atualizar')->addClass('button-l') }}
            </div>

        {{ html()->form()->close() }}

        <footer class="py-8 border-t border-brand-ligher">
            <a href="#" class="button button--secondary button-sm">{{ __('Exportar PDF') }}</a>
        </footer>
    @endcomponent    
@endsection