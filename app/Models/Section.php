<?php

namespace App\Models;

use App\Jobs\File\ReadFileCommand;
use App\Jobs\File\WriteFileCommand;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Bus;
use App\Models\Helpers\RoutesByHashid;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

/**
 * @property \Carbon\Carbon $deleted_at
 * @property mixed          $project
 * @property mixed          file
 * @property mixed          project_id
 */
class Section extends BaseModel
{
    use RoutesByHashid;

    protected $fillable = [
        'title',
        'status',
        'file_content',
        'project_id',
    ];

    /**
     * Override save method so we create the file for the section.
     *
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if (is_null($this->file)) {
            $this->setAttribute('file_content', [
                'ops' => [],
            ]);
        }

        return parent::save($options);
    }

    /**
     * @return BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Fetch content of the section file.
     *
     * @return Collection
     */
    public function getFileContentAttribute(): Collection
    {
        if (is_null($this->attributes['file'])) {
            return [];
        }

        $data = Bus::dispatchNow(new ReadFileCommand($this->file));

        if (is_null($data)) {
            return collect([
                'ops' => [],
            ]);
        }

        return collect(json_decode($data, true));
    }

    /**
     * Set content of the section file.
     *
     * @param array $data
     */
    public function setFileContentAttribute(array $data)
    {
        if (is_null($this->file)) {
            $this->attributes['file'] = $this->project_id . '/' . uniqid('sect-') . '.json';
        }

        Bus::dispatchNow(new WriteFileCommand($this->file, json_encode($data)));
    }

    /**
     * Truncates the section's title.
     *
     * @return void
     */
    public function getSmallTitleAttribute()
    {
        return Str::limit($this->attributes['title'], 120, '...');
    }
}
