<?php

namespace App\Models\Helpers;

trait RoutesByHashid
{
    /**
     * Define the Hash ID column as the one used for finding
     * the model wanted on URL.
     *
     * @return void
     */
    public function getRouteName()
    {
        return 'hashid';
    }
}
