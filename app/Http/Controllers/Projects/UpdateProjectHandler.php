<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class UpdateProjectHandler extends Controller
{
    public function __invoke(Request $request, Project $project)
    {
        $input = $request->validate([
            'name'        => 'required|max:255',
            'description' => 'nullable',
        ]);

        $project->fill($input);
        $project->save();

        return redirect()->route('projects.edit', $project);
    }
}
