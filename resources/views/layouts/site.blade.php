@include('layouts.partials.head')

<body class="h-screen">
    {{-- Top navbar --}}
    <div class="flex bg-white p-6 border border-grey-lighter justify-between items-center">
        <h1 class="font-light text-2xl">
            <a href="{{ route('home') }}" class="no-underline text-black uppercase">{{ config('app.name') }}</a>
        </h1>
        <nav class="flex-shrink">
            <a href="{{ route('login') }}" class="nav-link">{{ __('Login') }}</a>
            <a href="{{ route('register') }}" class="nav-link">{{ __('Register') }}</a>
        </nav>
    </div>
    <div id="app" class="min-h-full">
        @yield('content')
    </div>
</body>
</html>
