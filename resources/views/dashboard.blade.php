@extends('layouts.app')

@section('content')
    <main class="max-w-lg mx-auto">
        @component('comps.section-title') {{ __('Projetos') }} @endcomponent

        @forelse ($projects as $project)
            <a href="{{ route('projects.edit', $project) }}" class="text-black hover:text-brand-dark no-underline border-b border-brand-ligher py-4 w-auto flex">
                <div class="flex-shrink">
                    <span class="icon-basic-book project__icon text-5xl mr-4"></span>
                </div>
                <div class="flex-grow">
                    <div class="text-xl pb-2 font-normal">{{ $project->name }}</div>
                    <div class="text-grey-dark text-sm font-normal">{{ $project->description }}</div>
                </div>
            </a>
        @empty
            <p class="text-center my-8 text-grey-dark">
                {{ __('Você ainda não criou nenhum projeto.') }}
            </p>
        @endforelse

        <div class="text-center mt-8 pt-6">
            <a href="{{ route('projects.create') }}" class="button button-primary">{{ __('Adicionar projeto') }}</a>
        </div>
    </main>
@endsection