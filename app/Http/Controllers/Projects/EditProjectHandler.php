<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Models\Project;

class EditProjectHandler extends Controller
{
    public function __invoke(Project $project)
    {
        return view('projects.edit')->withProject($project);
    }
}
