<?php

namespace App\Http\Controllers\Sections;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;

class StoreSectionHandler extends Controller
{
    public function __invoke(Request $request, Project $project)
    {
        $input = $request->validate([
            'title' => 'required|max:255',
        ]);

        $section = $project->sections()->create($input);

        return redirect()->route('sections.edit', [$project, $section]);
    }
}
