
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue')

/**
 * Next, we will load Turbolinks and Turbolink adapter, so we can restart
 * our Vue instance when Turbolinks update the page.
 */

import Turbolinks from 'turbolinks'
import TurbolinksAdapter from 'vue-turbolinks'

Turbolinks.start()

Vue.use(TurbolinksAdapter)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('editor', require('./components/Editor.vue'))

document.addEventListener('turbolinks:load', () => {
    const elem = document.getElementById('app')

    if (elem === null) {
        return
    }

    const app = new Vue({
        el: '#app'
    })
})