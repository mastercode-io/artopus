<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 31/08/17
 * Time: 02:10
 */

namespace App\Exceptions;


class ModelUpdateException extends \Exception
{
    protected $message = 'Could not update the entity';

}