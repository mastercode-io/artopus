<?php

namespace App\Http\Controllers\Sections;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Section;
use Illuminate\Http\Request;
use App\Http\Resources\SectionResource;

class UpdateSectionHandler extends Controller
{
    public function __invoke(Request $request, Project $project, Section $section)
    {
        $input = $request->validate([
            'title'        => 'required|max:255',
            'file_content' => 'required|array',
        ]);

        $section->fill($input);
        $section->save();

        return new SectionResource($section);
    }
}
