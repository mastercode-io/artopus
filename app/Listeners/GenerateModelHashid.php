<?php

namespace App\Listeners;

use App\Models\Contracts\HasHashids;

class GenerateModelHashid
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @param         $models
     * @return void
     */
    public function handle($event, $models)
    {
        if ($models[0] instanceof HasHashids) {
            $models[0]->makeHashid();
            $models[0]->save();
        }
    }
}
