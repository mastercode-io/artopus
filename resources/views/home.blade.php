@extends('layouts.site')

@section('content')
    <main class="min-h-full flex justify-center">
        <!-- /hero -->
        @if (session('status'))
            <div class="bg-green-lightest border border-green-light text-green-dark text-sm px-4 py-3 rounded mb-4">
                {{ session('status') }}
            </div>
        @endif
        <!-- Hero -->
        <div class="text-center hero--home">
            <h1 class="text-4xl leading-loose font-light mb-8 text-grey-darker">
                {{ __('Não perca tempo com formatações.') }}<br>
                <span class="fw7">{{ __('Foque na pesquisa.') }}</span>
            </h1>
            <a href="{{ route('register') }}" class="button my-8">
                {{ __('Começar gratuitamente') }}
            </a>
        </div>
    </main>
@endsection