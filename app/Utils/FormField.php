<?php

namespace App\Utils;

use \Spatie\Html\Html;
use \Spatie\Html\Elements\Div;
use \Spatie\Html\HtmlElement;
use \Illuminate\Support\ViewErrorBag;

class FormField
{
    /** @var Html */
    protected $html;

    /** @var bool */
    protected $required = false;

    public function __construct(Html $html)
    {
        $this->html = $html;
    }

    /**
     * Set the field as required.
     *
     * @return void
     */
    public function required()
    {
        $formBuilder = clone $this;
        $formBuilder->required = true;

        return $formBuilder;
    }

    /**
     * Render the field wrapper.
     */
    protected function wrapper(string $field): Div
    {
        return $this->html->div()->class('field mb-4')->addClass(session()->get($field) ? 'field--error' : '');
    }

    /**
     * Assemble all the input html.
     *
     * @param string $field
     * @param string $label
     * @param HtmlElement $contents
     * @return Div
     */
    protected function assemble(string $field, string $label, HtmlElement $contents): Div
    {
        return $this->wrapper($field)->children([
            $this->html->label(__($label), $field)->class('field__label'),
            $contents->placeholder($label)->class('field__input'),
            $this->error($field),
        ]);
    }

    /**
     * Render an error alert to the specified field.
     *
     * @param string $field
     * @return string
     */
    protected function error(string $field): string
    {
        $errors = session('errors', new ViewErrorBag());
        $alert = '<span class="text-red-dark text-sm mt-2">:message</span>';

        return $errors->first($field, $alert);
    }

    /**
     * Render a email input field.
     *
     * @param string $field
     * @param string $label
     * @param string|null $value
     * @return Div
     */
    public function email(string $field, string $label, ?string $value = null): Div
    {
        return $this->assemble($field, $label, $this->html->email($field, $value));
    }

    /**
     * Render a text input field.
     *
     * @param string $field
     * @param string $label
     * @param string|null $value
     * @return Div
     */
    public function text(string $field, string $label, ?string $value = null): Div
    {
        return $this->assemble($field, $label, $this->html->text($field, $value));
    }

    /**
     * Render a password input field.
     *
     * @param string $field
     * @param string $label
     * @return Div
     */
    public function password(string $field, string $label): Div
    {
        return $this->assemble($field, $label, $this->html->password($field));
    }

    /**
     * Render button to submit form.
     *
     * @param string $label
     * @return void
     */
    public function submit(string $label)
    {
        return $this->html->submit(__($label))->class('button button-primary');
    }
}
