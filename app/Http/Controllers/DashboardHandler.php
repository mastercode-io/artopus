<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class DashboardHandler extends Controller
{
    public function __invoke(Request $request, Project $projects)
    {
        $items = $projects->getByUserId($request->user()->getKey());

        return view('dashboard')->with('projects', $items);
    }
}
