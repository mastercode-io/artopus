<?php

/*
 * Authentication routes.
 */

 Auth::routes();

/*
 * Website routes.
 */

Route::get('/', 'HomeHandler')->name('home')->middleware('guest');

/*
 * Application routes.
 */

Route::prefix('app')->middleware('auth')->group(function () {
    Route::get('/', 'DashboardHandler')->name('dashboard');

    // Projects
    Route::get('/new', 'Projects\CreateProjectHandler')->name('projects.create');
    Route::post('/new', 'Projects\StoreProjectHandler')->name('projects.store');
    Route::get('/{project}', 'Projects\EditProjectHandler')->name('projects.edit');
    Route::put('/{project}', 'Projects\UpdateProjectHandler')->name('projects.update');
    Route::delete('/{project}', 'Projects\DeleteProjectHandler')->name('projects.delete');

    // Sections
    Route::post('/{project}/sections', 'Sections\StoreSectionHandler')->name('sections.store');
    Route::get('/{project}/sections/{section}', 'Sections\EditSectionHandler')->name('sections.edit');
    ROute::patch('/{project}/sections/{section}', 'Sections\UpdateSectionHandler')->name('sections.update');
});
