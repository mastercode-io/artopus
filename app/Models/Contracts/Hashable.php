<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 03/10/17
 * Time: 01:34
 */

namespace App\Models\Contracts;

use Hashids\Hashids;

trait Hashable
{
    /**
     * Generates a Hash Id based on model's id.
     *
     * @return string
     */
    public function makeHashid(): string
    {
        $hashids = new Hashids($this->table, 10);

        if (!$this->getKey()) {
            return null;
        }

        $this->hashid = $hashids->encode($this->getKey());

        return $this->hashid;
    }

    /**
     * Check if the provided hashid matches the model's id.
     *
     * @param string $hashid
     * @return bool
     */
    public function checkHashid(string $hashid): bool
    {
        if (!$this->getKey()) {
            return false;
        }

        $hashids = new Hashids($this->table);

        return $this->getKey() === $hashids->decode($hashid);
    }

    /**
     * Determine the property used in route binding.
     *
     * @return void
     */
    public function getRouteKeyName()
    {
        return 'hashid';
    }
}
