@include('layouts.partials.head')

<body class="h-screen">

    {{-- Top navbar --}}
    <div class="flex bg-white p-4 border border-grey-lighter justify-between items-center">
        <h1 class="font-light text-2xl">
            <a href="{{ route('home') }}" class="no-underline text-black uppercase">{{ config('app.name') }}</a>
        </h1>
        <nav class="flex-shrink">
            <a href="#" class="nav-link">{{ Auth::user()->name }}</a>
            <a class="nav-link" href="{{ route('logout') }}"
                                class="no-underline hover:underline text-grey-darker text-sm"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                {{ __('Sair') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </nav>
    </div>
    <!-- /hero -->
    <div id="app" class="min-h-full mt-8">
        @if (session('success'))
            <div class="bg-green-lightest border border-green-light text-green-dark text-sm px-4 py-3 rounded mb-4">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="bg-red-lightest border border-red-light text-red-dark text-sm px-4 py-3 rounded mb-4">
                {{ session('error') }}
            </div>
        @endif

        @yield('content')
    </div>
</body>
</html>
