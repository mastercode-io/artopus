<?php

namespace App\Models\Helpers;

trait HasUserComparisons
{
    /**
     * Determine if the project is owned by the provided user.
     *
     * @param int $userId
     * @return bool
     */
    public function isOwnedBy(int $userId): bool
    {
        return $this->owner_id === $userId;
    }
}
