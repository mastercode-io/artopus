@extends('layouts.app')

@section('content')
    <div class="max-w-2xl mx-auto">
        @component('comps.section-title') {{ $project->name }} @endcomponent
    </div>
    @component('comps.content-editing', ['project' => $project])

        <editor data-api="{{ route('sections.update', [$project, $section]) }}" data-title="{{ $section->title }}" data-content="{{ $section->file_content->toJson() }}"></editor>

    @endcomponent    
@endsection